package com.goodselection.common.utils;

import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @author YaoXunYu
 * created on 10/29/2018
 */
public class DateTimeUtils {

    public static LocalDateTime currentHour() {
        return LocalDateTime.now()
          .withNano(0)
          .withSecond(0)
          .withMinute(0);
    }

    public static LocalDate currentMonth() {
        return LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
    }

    public static LocalDateTime currentMonthTime() {
        return LocalDateTime.of(currentMonth(), LocalTime.MIDNIGHT);
    }

    public static Stream<Tuple2<LocalDate, LocalDate>> dailySplitStream(LocalDate start, LocalDate end) {
        Iterator<Tuple2<LocalDate, LocalDate>> iterator = new Iterator<Tuple2<LocalDate, LocalDate>>() {
            private LocalDate s = start;
            private LocalDate e = end;

            @Override
            public boolean hasNext() {
                return s.isBefore(e);
            }

            @Override
            public Tuple2<LocalDate, LocalDate> next() {
                LocalDate exchange = s.plusDays(1);
                Tuple2<LocalDate, LocalDate> tuple = Tuples.of(s, exchange);
                s = exchange;
                return tuple;
            }
        };

        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator,
          Spliterator.ORDERED | Spliterator.IMMUTABLE),
          false);
    }

    public static Stream<Tuple2<LocalDateTime, LocalDateTime>> dailySplitStream(LocalDateTime start, LocalDateTime end) {
        Iterator<Tuple2<LocalDateTime, LocalDateTime>> iterator = new Iterator<Tuple2<LocalDateTime, LocalDateTime>>() {
            private LocalDateTime s = start;
            private LocalDateTime e = end;

            @Override
            public boolean hasNext() {
                return s.isBefore(e);
            }

            @Override
            public Tuple2<LocalDateTime, LocalDateTime> next() {
                LocalDateTime sDay = s.toLocalDate().plusDays(1).atStartOfDay();
                if (!sDay.isBefore(e)) {
                    Tuple2<LocalDateTime, LocalDateTime> tuple = Tuples.of(s, e);
                    s = e;
                    return tuple;
                }
                if (s.isBefore(sDay)) {
                    Tuple2<LocalDateTime, LocalDateTime> tuple = Tuples.of(s, sDay);
                    s = sDay;
                    return tuple;
                } else {
                    LocalDateTime exchange = s.plusDays(1);
                    Tuple2<LocalDateTime, LocalDateTime> tuple = Tuples.of(s, exchange);
                    s = exchange;
                    return tuple;
                }
            }
        };

        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator,
          Spliterator.ORDERED | Spliterator.IMMUTABLE),
          false);
    }

    public static Stream<Tuple2<LocalDateTime, LocalDateTime>> dailySplitStreamWithTime(LocalDate start, LocalDate end) {
        Iterator<Tuple2<LocalDateTime, LocalDateTime>> iterator = new Iterator<Tuple2<LocalDateTime, LocalDateTime>>() {
            private LocalDateTime s = start.atStartOfDay();
            private LocalDateTime e = end.atStartOfDay();

            @Override
            public boolean hasNext() {
                return s.isBefore(e);
            }

            @Override
            public Tuple2<LocalDateTime, LocalDateTime> next() {
                LocalDateTime exchange = s.plusDays(1);
                Tuple2<LocalDateTime, LocalDateTime> tuple = Tuples.of(s, exchange);
                s = exchange;
                return tuple;
            }
        };

        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator,
          Spliterator.ORDERED | Spliterator.IMMUTABLE),
          false);
    }
}
