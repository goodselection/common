package com.goodselection.common.utils;

/**
 * @author YaoXunYu
 * created on 02/19/2019
 */
public class ByteBooleanConverter {

    public static byte convert(boolean b) {
        return (byte) (b ? 1 : 0);
    }

    public static boolean convert(byte b) {
        return b == 1;
    }
}
