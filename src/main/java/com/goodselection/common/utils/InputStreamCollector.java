package com.goodselection.common.utils;

import java.io.InputStream;
import java.io.SequenceInputStream;

/**
 * @author YaoXunYu
 * created on 11/14/2018
 */
public class InputStreamCollector {
    private InputStream is;

    public void collect(InputStream is) {
        if (this.is == null) {
            this.is = is;
        } else {
            this.is = new SequenceInputStream(this.is, is);
        }
    }

    public InputStream getInputStream() {
        return this.is;
    }
}
