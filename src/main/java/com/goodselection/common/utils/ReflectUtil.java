package com.goodselection.common.utils;

import static java.util.Locale.ENGLISH;

/**
 * @author YaoXunYu
 * created on 11/02/2018
 */
public class ReflectUtil {
    public static final String GET_PREFIX = "get";
    public static final String SET_PREFIX = "set";

    public static String capitalize(String name) {
        if (name == null || name.length() == 0) {
            return name;
        }
        return name.substring(0, 1).toUpperCase(ENGLISH) + name.substring(1);
    }

    public static String getter(String name) {
        return GET_PREFIX + capitalize(name);
    }

    public static String setter(String name) {
        return SET_PREFIX + capitalize(name);
    }

    public static String properties(String methodName) {
        return methodName.substring(3, 4).toLowerCase(ENGLISH) + methodName.substring(4);
    }
}
