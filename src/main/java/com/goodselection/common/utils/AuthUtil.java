package com.goodselection.common.utils;

import com.goodselection.common.domains.auth.JWTUserAuthentication;
import com.goodselection.common.exceptions.AuthenticationException;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.function.Function;

/**
 * @author YaoXunYu
 * 该工具类并没有异常处理，确保传过来的Basic和Bearer是正确的值
 * created on 02/25/2019
 */
public class AuthUtil {
    private static final String BASIC = "Basic ";

    private static final String BEARER = "Bearer ";

    private static final String SEPARATOR = ":";

    public static String basicEncode(String username, String password) {
        String s = username + SEPARATOR + password;
        byte[] b = Base64.getEncoder().encode(s.getBytes(StandardCharsets.UTF_8));
        return BASIC + new String(b, StandardCharsets.UTF_8);
    }

    public static String[] basicDecode(String basic) {
        String s = basic.substring(6);
        byte[] bytes = Base64.getDecoder().decode(s);
        return new String(bytes, StandardCharsets.UTF_8).split(":");
    }

    public static String bearerEncode(String bearer) {
        return BEARER + bearer;
    }

    public static String bearerDecode(String bearer) {
        return bearer.substring(7);
    }

    public static String getAuth(ServerWebExchange exchange) {
        ServerHttpRequest request = exchange.getRequest();
        String auth = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        if (auth == null) {
            HttpCookie authorizationCookie = request.getCookies().getFirst(HttpHeaders.AUTHORIZATION);
            if (authorizationCookie != null) {
                auth = authorizationCookie.getValue();
            } else {
                throw new AuthenticationException("Authorization not found");
            }
        }
        return auth;
    }

    public static Mono<JWTUserAuthentication> authenticate(String auth,
                                                           Function<String, Mono<JWTUserAuthentication>> httpBasicFn,
                                                           Function<String, Mono<JWTUserAuthentication>> bearerFn) {
        if (auth.startsWith(BASIC)) {
            return httpBasicFn.apply(auth);
        } else if (auth.startsWith(BEARER)) {
            return bearerFn.apply(auth);
        } else {
            throw new AuthenticationException("Unsupported token");
        }
    }
}
