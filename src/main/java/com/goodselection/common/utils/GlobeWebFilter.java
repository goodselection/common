package com.goodselection.common.utils;

import com.goodselection.common.exceptions.HttpStatusCodeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.InvalidPropertyException;
import org.springframework.boot.web.reactive.filter.OrderedWebFilter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.cors.reactive.CorsUtils;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilterChain;
import org.springframework.web.server.handler.DefaultWebFilterChain;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author YaoXunYu
 * created on 01/09/2019
 */
public class GlobeWebFilter implements OrderedWebFilter {
    private static final Logger logger = LoggerFactory.getLogger(GlobeWebFilter.class);
    private final List<String> plain = Collections.singletonList("text/plain;charset=UTF-8");
    private final AntPathMatcher matcher = new AntPathMatcher();
    private BiFunction<ServerWebExchange, ? super Throwable, Mono<Void>> errorHandler = (exchange, throwable) -> {
        ServerHttpResponse response = exchange.getResponse();
        response.getHeaders()
          .put(HttpHeaders.CONTENT_TYPE, plain);
        if (throwable instanceof HttpStatusCodeException) {
            HttpStatusCodeException e = (HttpStatusCodeException) throwable;
            response.setStatusCode(e.getStatus());
        } else if (throwable instanceof ResponseStatusException) {
            ResponseStatusException e = (ResponseStatusException) throwable;
            response.setStatusCode(e.getStatus());
        } else if (throwable instanceof InvalidPropertyException) {
            response.setStatusCode(HttpStatus.BAD_REQUEST);
        } else {
            response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
            logger.trace("unexpected error", throwable);
        }
        return response.writeWith(Mono.just(response.bufferFactory().wrap(throwable.getMessage().getBytes(StandardCharsets.UTF_8))));
    };
    private final String access_control_allow_methods = Stream
      .of(HttpMethod.GET, HttpMethod.POST, HttpMethod.PUT, HttpMethod.DELETE, HttpMethod.PATCH)
      .map(Enum::name)
      .collect(Collectors.joining(","));
    private final int order;
    private final Set<String> pass;

    public GlobeWebFilter(int order, Set<String> pass) {
        this.order = order;
        this.pass = Collections.unmodifiableSet(pass);
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain c) {
        DefaultWebFilterChain chain = (DefaultWebFilterChain) c;
        ServerHttpRequest request = exchange.getRequest();

        //cors
        if (CorsUtils.isCorsRequest(request)) {
            ServerHttpResponse response = exchange.getResponse();
            HttpHeaders headers = response.getHeaders();
            headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
            headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, HttpHeaders.AUTHORIZATION);
            headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, access_control_allow_methods);
            headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, request.getHeaders().getFirst(HttpHeaders.ORIGIN));
            //允许header中获取
            headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.AUTHORIZATION);
            if (CorsUtils.isPreFlightRequest(request)) {
                headers.add(HttpHeaders.ACCESS_CONTROL_MAX_AGE, "86400");
                response.setStatusCode(HttpStatus.OK);
                return response.setComplete();
            }
        }

        //always allow
        RequestPath path = request.getPath();
        if (pass.stream()
          .anyMatch(ap -> matcher.match(ap, path.value()))) {
            return chain.getHandler()
              .handle(exchange)
              .onErrorResume(throwable -> errorHandler.apply(exchange, throwable));
        }

        //next filter
        return chain.filter(exchange)
          .onErrorResume(throwable -> errorHandler.apply(exchange, throwable));
    }

    @Override
    public int getOrder() {
        return order;
    }
}
