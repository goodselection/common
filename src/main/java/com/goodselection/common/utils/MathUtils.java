package com.goodselection.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;

/**
 * @author YaoXunYu
 * created on 10/11/2018
 */
public class MathUtils {

    private static final BigDecimal HUNDRED = BigDecimal.TEN.multiply(BigDecimal.TEN);

    /**
     * @param dividend NullAble
     * @param divisor  NullAble
     * @return might be ZERO
     */
    public static float percent(BigDecimal dividend, BigDecimal divisor) {
        if (divisor.compareTo(BigDecimal.ZERO) != 0) {
            return dividend.multiply(HUNDRED)
              .divide(divisor, RoundingMode.HALF_EVEN)
              .floatValue();
        } else {
            return 0f;
        }
    }

    /**
     * @param dividend NullAble
     * @param divisor  NullAble
     * @return might be null
     */
    public static Float percent(Integer dividend, Integer divisor) {
        if (dividend != null && divisor != null && divisor != 0) {
            float f = dividend.floatValue() / divisor.floatValue() * 100;
            return (float) Math.round(f * 100) / 100;
        } else {
            return null;
        }
    }

    /**
     * @param dividend NullAble
     * @param divisor  NullAble
     * @return might be ZERO
     */
    public static float percent(float dividend, float divisor) {
        if (divisor != 0) {
            float f = dividend / divisor * 100;
            return (float) Math.round(f * 100) / 100;
        } else {
            return 0f;
        }
    }

    /**
     * warning: no check for null point or zero
     */
    public static BigDecimal proportion(BigDecimal dividend, BigDecimal divisor) {
        return dividend.subtract(divisor)
          .divide(divisor, 4, RoundingMode.HALF_EVEN);
    }

    public static double proportion(double dividend, double divisor) {
        return (dividend - divisor) / divisor;
    }

    public static Integer nullableAdd(Integer i1, Integer i2) {
        if (i1 != null) {
            return i2 != null ? Integer.valueOf(i1 + i2) : i1;
        } else {
            return i2;
        }
    }

    public static Float nullableAdd(Float i1, Float i2) {
        if (i1 != null) {
            return i2 != null ? Float.valueOf(i1 + i2) : i1;
        } else {
            return i2;
        }
    }

    public static Double nullableAdd(Double i1, Double i2) {
        if (i1 != null) {
            return i2 != null ? Double.valueOf(i1 + i2) : i1;
        } else {
            return i2;
        }
    }

    /**
     * Warning: once use
     */
    public static NumberFormat percentFormat() {
        NumberFormat percentFormat = NumberFormat.getPercentInstance();
        percentFormat.setRoundingMode(RoundingMode.HALF_EVEN);
        percentFormat.setMaximumFractionDigits(2);
        return percentFormat;
    }
}
