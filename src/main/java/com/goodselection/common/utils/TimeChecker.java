package com.goodselection.common.utils;

import com.goodselection.common.exceptions.BadParameterException;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author LongTao
 * created on 2019/1/2
 */
public class TimeChecker {
    //校验开始时间结束时间
    public static void check(LocalDateTime start, LocalDateTime end) {
        checkStart(start, end);
        checkEnd(end);
    }

    public static void check(LocalDate start, LocalDate end) {
        checkStart(start, end);
        checkEnd(end);
    }

    //报表导出开始日期大于或等于结束日期
    public static void checkStart(LocalDateTime start, LocalDateTime end) {
        if (!start.isBefore(end)) {
            throw new BadParameterException("报表导出开始日期大于或等于结束日期");
        }
    }

    public static void checkStart(LocalDate start, LocalDate end) {
        if (!start.isBefore(end)) {
            throw new BadParameterException("报表导出开始日期大于或等于结束日期");
        }
    }

    //报表导出结束日期大于当前时间
    public static void checkEnd(LocalDateTime end) {
        LocalDateTime now = LocalDateTime.now();
        if (end.isAfter(now)) {
            throw new BadParameterException("报表导出结束日期大于当前时间");
        }
    }

    public static void checkEnd(LocalDate end) {
        LocalDate today = LocalDate.now();
        if (end.isAfter(today)) {
            throw new BadParameterException("报表导出结束日期大于当前时间");
        }
    }

}
