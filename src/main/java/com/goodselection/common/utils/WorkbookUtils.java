package com.goodselection.common.utils;

import com.goodselection.common.constants.ExtensionMediaType;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.core.io.buffer.DefaultDataBuffer;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.function.Function;

/**
 * @author LongTao
 * created on 2018/11/09
 */
public class WorkbookUtils {

    public static final Function<Workbook, DefaultDataBuffer> WRITE_TO_DATABUFFER = workbook -> {
        DefaultDataBuffer dataBuffer = new DefaultDataBufferFactory().allocateBuffer();
        try (OutputStream outputStream = dataBuffer.asOutputStream()) {
            workbook.write(outputStream);
        } catch (IOException ignore) {
        }
        return dataBuffer;
    };

    public static void setXLSXHeader(ServerHttpResponse response, String xlsxName) {
        HttpHeaders httpHeaders = response.getHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, ExtensionMediaType.XLSX);
        httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + encodeHttpHeader(xlsxName));
        //response.setStatusCode(HttpStatus.OK);
    }

    public static void setXLSHeader(ServerHttpResponse response, String xlsName) {
        HttpHeaders httpHeaders = response.getHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, ExtensionMediaType.XLS);
        httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + encodeHttpHeader(xlsName));
        //response.setStatusCode(HttpStatus.OK);
    }

    private static String encodeHttpHeader(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException ignore) {
        }
        return s;
    }
}
