package com.goodselection.common.utils;

import com.goodselection.common.annotations.WorkbookTitle;
import com.google.common.collect.ImmutableMap;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class SheetWriter<T> {

    private final ImmutableMap<Integer, Tuple2<WorkbookTitle, Method>> map;

    public SheetWriter(Class<T> clazz) {
        ImmutableMap.Builder<Integer, Tuple2<WorkbookTitle, Method>> builder = ImmutableMap.builder();
        Field[] fields = clazz.getDeclaredFields();
        int index = 0;
        for (Field field : fields) {
            WorkbookTitle workbookTitle = field.getAnnotation(WorkbookTitle.class);
            if (workbookTitle != null) {
                String getterName = ReflectUtil.getter(field.getName());
                try {
                    Method getter = clazz.getMethod(getterName);
                    builder.put(index++, Tuples.of(workbookTitle, getter));
                } catch (NoSuchMethodException ignore) {
                }
            }
        }
        map = builder.build();
    }

    /**
     * @return rowIndex
     */
    public int writeSheet(XSSFWorkbook workbook, String sheetName, Collection<T> rowData) {
        XSSFSheet sheet = workbook.createSheet(sheetName);
        return writeSheet(sheet, rowData);
    }

    /**
     * @param rowData sequential stream
     * @return rowIndex
     */
    public int writeSheet(XSSFWorkbook workbook, String sheetName, Stream<T> rowData) {
        XSSFSheet sheet = workbook.createSheet(sheetName);
        return writeSheet(sheet, rowData);
    }

    public int writeSheet(XSSFSheet sheet, Collection<T> rowData) {
        writeTitle(sheet);
        return writeDataRows(sheet, rowData);
    }

    public int writeSheet(XSSFSheet sheet, Stream<T> rowData) {
        writeTitle(sheet);
        return writeDataRows(sheet, rowData);
    }

    /**
     * write row1 as title
     */
    public void writeTitle(XSSFSheet sheet) {
        XSSFRow titleRow = sheet.createRow(0);
        map.forEach((index, tuple) -> {
            XSSFCell cell = titleRow.createCell(index);
            cell.setCellValue(tuple.getT1().value());
        });
    }

    public int writeDataRows(XSSFSheet sheet, Stream<T> stream) {
        AtomicInteger rowIndex = new AtomicInteger(1);
        stream.sequential()
          .forEach(value -> {
              XSSFRow row = sheet.createRow(rowIndex.getAndIncrement());
              writeRow(row, value);
          });
        return rowIndex.get();
    }

    public int writeDataRows(XSSFSheet sheet, Collection<T> rowData) {
        int rowIndex = 1;
        for (T value : rowData) {
            XSSFRow row = sheet.createRow(rowIndex++);
            writeRow(row, value);
        }
        return rowIndex;
    }

    private void writeRow(XSSFRow row, T value) {
        map.forEach((index, tuple) -> {
            XSSFCell c = row.createCell(index);
            Method getter = tuple.getT2();
            try {
                Object o = getter.invoke(value);
                if (o != null) {
                    if (o instanceof Double) {
                        c.setCellValue((Double) o);
                    } else {
                        c.setCellValue(String.valueOf(o));
                    }
                }
            } catch (IllegalAccessException | InvocationTargetException ignore) {
            }
        });
    }
}
