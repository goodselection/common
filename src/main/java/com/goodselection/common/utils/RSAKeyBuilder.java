package com.goodselection.common.utils;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * @author YaoXunYu
 * created on 02/21/2019
 */
public class RSAKeyBuilder {

    public static RSAPublicKey buildPublic(String publicKey, String... errorMsg) {
        try {
            byte[] key = Base64.getDecoder().decode(publicKey);
            X509EncodedKeySpec spec = new X509EncodedKeySpec(key);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return (RSAPublicKey) keyFactory.generatePublic(spec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            if (errorMsg != null) {
                throw new RuntimeException(errorMsg[0], e);
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    public static RSAPrivateKey buildPrivate(String privateKey, String... errorMsg) {
        try {
            byte[] key = Base64.getDecoder().decode(privateKey);
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(key);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return (RSAPrivateKey) keyFactory.generatePrivate(spec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            if (errorMsg != null) {
                throw new RuntimeException(errorMsg[0], e);
            } else {
                throw new RuntimeException(e);
            }
        }
    }
}
