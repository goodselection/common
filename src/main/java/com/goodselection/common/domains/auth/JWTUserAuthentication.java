package com.goodselection.common.domains.auth;

import com.goodselection.common.domains.user.UserRoles;

import java.security.Principal;

/**
 * @author YaoXunYu
 * created on 12/21/2018
 */
public class JWTUserAuthentication extends UserRoles implements Principal {
    private static final String NAME = "JWT";

    private String token;

    public JWTUserAuthentication(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public boolean equals(Object another) {
        if (another instanceof JWTUserAuthentication) {
            JWTUserAuthentication a = (JWTUserAuthentication) another;
            return this.token.equals(a.token);
        }
        return false;
    }

    @Override
    public String toString() {
        return this.token;
    }

    @Override
    public int hashCode() {
        return token.hashCode();
    }

    @Override
    public String getName() {
        return NAME;
    }
}
