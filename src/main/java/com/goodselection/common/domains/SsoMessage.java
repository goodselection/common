package com.goodselection.common.domains;

import com.alibaba.fastjson.JSONObject;
import com.goodselection.common.constants.RRConstants;
import com.goodselection.common.constants.RRMethod;

import java.util.function.Predicate;

/**
 * @author YaoXunYu
 * created on 01/29/2019
 */
public class SsoMessage<T> {
    private String route;
    private RRMethod method;
    private T resource;
    private Predicate<Integer> test;

    public static String wrapper(String route, RRMethod method, Object resource) {
        return new JSONObject()
          .fluentPut(RRConstants.ROUTE, route)
          .fluentPut(RRConstants.METHOD, method)
          .fluentPut(RRConstants.RESOURCE, resource)
          .toJSONString();
    }

    public String getMessage() {
        return wrapper(route, method, resource);
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public RRMethod getMethod() {
        return method;
    }

    public void setMethod(RRMethod method) {
        this.method = method;
    }

    public T getResource() {
        return resource;
    }

    public void setResource(T resource) {
        this.resource = resource;
    }

    public Predicate<Integer> getTest() {
        return test;
    }

    /**
     * @param test (integer)
     *             integer: sessionAppId
     */
    public void setTest(Predicate<Integer> test) {
        this.test = test;
    }

    public boolean test(Integer appId) {
        return test.test(appId);
    }
}
