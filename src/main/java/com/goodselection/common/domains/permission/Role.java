package com.goodselection.common.domains.permission;

/**
 * @author YaoXunYu
 * created on 01/08/2019
 */
public class Role {
    private Integer roleId;
    private String role;
    private boolean frozen;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isFrozen() {
        return frozen;
    }

    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }
}
