package com.goodselection.common.domains.permission;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author YaoXunYu
 * created on 12/25/2018
 */
public class RolePermissions extends Role {
    private Set<Permission> permissions = new HashSet<>();

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Collection<Permission> permissions) {
        this.permissions = new HashSet<>(permissions);
    }

    public void addPermission(Permission permission) {
        this.permissions.add(permission);
    }

    public void removePermission(Permission permission) {
        this.permissions.remove(permission);
    }
}