package com.goodselection.common.domains.permission;

import org.springframework.http.HttpMethod;
import org.springframework.http.server.RequestPath;
import org.springframework.util.AntPathMatcher;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author YaoXunYu
 * created on 01/17/2019
 */
public class Permission {
    private final static AntPathMatcher MATCHER = new AntPathMatcher();
    private final static String SEPARATOR = ",";
    private Integer permissionId;
    private Integer appId;
    private boolean frozen;
    private String antPath;
    private Set<HttpMethod> httpMethods = new HashSet<>();

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public boolean isFrozen() {
        return frozen;
    }

    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }

    public String getAntPath() {
        return antPath;
    }

    public void setAntPath(String antPath) {
        this.antPath = antPath;
    }

    public Set<String> getHttpMethods() {
        return httpMethods
          .stream()
          .map(Enum::name)
          .collect(Collectors.toSet());
    }

    public void setHttpMethods(Collection<String> httpMethods) {
        this.httpMethods.clear();
        httpMethods.forEach(str -> {
            HttpMethod m = HttpMethod.resolve(str);
            if (m != null) {
                this.httpMethods.add(m);
            }
        });
    }

    public String httpMethodStr() {
        return httpMethods
          .stream()
          .map(Enum::name)
          .collect(Collectors.joining(SEPARATOR));
    }

    public void setHttpMethodStr(String httpMethodStr) {
        this.httpMethods.clear();
        Arrays.stream(httpMethodStr.split(SEPARATOR))
          .forEach(str -> {
              HttpMethod m = HttpMethod.resolve(str);
              if (m != null) {
                  this.httpMethods.add(m);
              }
          });
    }

    public boolean match(RequestPath path, HttpMethod method) {
        return matchPath(path) && matchMethod(method);
    }

    private boolean matchPath(RequestPath path) {
        if (antPath != null) {
            return MATCHER.match(antPath, path.value());
        } else {
            return false;
        }
    }

    private boolean matchMethod(HttpMethod method) {
        return httpMethods.contains(method);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Permission) {
            Permission p = (Permission) o;
            return this.permissionId.equals(p.permissionId)
              && this.appId.equals(p.appId)
              && this.frozen == p.frozen
              && this.antPath.equals(p.antPath)
              && this.httpMethods.equals(p.httpMethods);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(permissionId, appId, frozen, antPath, httpMethods);
    }
}
