package com.goodselection.common.domains.user;

/**
 * @author YaoXunYu
 * created on 01/04/2019
 */
public class UserSimple extends UserModifiable {
    private Long userId;
    private String username;
    private boolean frozen;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isFrozen() {
        return frozen;
    }

    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }
}
