package com.goodselection.common.domains.user;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author YaoXunYu
 * created on 01/04/2019
 */
public class UserRoles extends UserSimple {
    private Set<String> roles = new HashSet<>();

    public Set<String> getRoles() {
        return roles;
    }

    public String[] getRolesAsArray() {
        return roles.toArray(new String[]{});
    }

    public void setRoles(Collection<String> roles) {
        this.roles = new HashSet<>(roles);
    }

    public void addRole(String role) {
        this.roles.add(role);
    }

    public void removeRole(String role) {
        this.roles.remove(role);
    }
}
