package com.goodselection.common.domains.user;

import com.goodselection.common.utils.ReflectUtil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author YaoXunYu
 * created on 01/07/2019
 */
public class UserModifiable {

    private static final Map<String, Method> getters;

    static {
        getters = new HashMap<>();
        Method[] methods = UserModifiable.class.getDeclaredMethods();
        for (Method m : methods) {
            if (m.getName().startsWith(ReflectUtil.GET_PREFIX)) {
                String name = ReflectUtil.properties(m.getName());
                getters.put(name, m);
            }
        }
    }

    public Map<String, Object> toMapWithoutNullProperties() {
        Map<String, Object> map = new HashMap<>();
        getters.forEach((name, method) -> {
            try {
                Object o = method.invoke(this);
                if (o != null) {
                    map.put(name, o);
                }
            } catch (IllegalAccessException | InvocationTargetException ignore) {
            }
        });
        return map;
    }

    private String nickname;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
