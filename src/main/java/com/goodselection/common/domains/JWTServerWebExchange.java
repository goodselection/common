package com.goodselection.common.domains;

import com.goodselection.common.domains.auth.JWTUserAuthentication;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.ServerWebExchangeDecorator;
import reactor.core.publisher.Mono;

/**
 * @author YaoXunYu
 * created on 12/21/2018
 */
public class JWTServerWebExchange extends ServerWebExchangeDecorator {

    private final JWTUserAuthentication authentication;

    public JWTServerWebExchange(ServerWebExchange delegate, JWTUserAuthentication authentication) {
        super(delegate);
        this.authentication = authentication;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Mono<JWTUserAuthentication> getPrincipal() {
        return Mono.just(authentication);
    }

    public JWTUserAuthentication getAuthentication() {
        return authentication;
    }
}
