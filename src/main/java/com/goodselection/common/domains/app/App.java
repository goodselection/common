package com.goodselection.common.domains.app;

/**
 * @author YaoXunYu
 * created on 02/21/2019
 */
public class App {
    protected Integer appId;
    protected String appName;

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
}
