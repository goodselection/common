package com.goodselection.common.constants;

/**
 * @author YaoXunYu
 * created on 11/14/2018
 */
public interface ExtensionMediaType {
    String XLS = "application/vnd.ms-excel";
    String XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    //String DOC = "application/msword";
    //String DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
}
