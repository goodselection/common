package com.goodselection.common.constants;

/**
 * @author YaoXunYu
 * created on 02/11/2019
 * remote route method, inspire from restful.
 * PUT: put ...
 * DELETE: delete ...
 * POST: replace all of ...
 * others: not used
 */
public enum RRMethod {
    GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE;
}
