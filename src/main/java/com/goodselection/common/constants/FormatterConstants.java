package com.goodselection.common.constants;

import java.time.format.DateTimeFormatter;

/**
 * @author YaoXunYu
 * created on 11/27/2018
 */
public interface FormatterConstants {
    DateTimeFormatter NORMAL = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    DateTimeFormatter HOUR = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH");
    DateTimeFormatter MONTH = DateTimeFormatter.ofPattern("yyyy-MM");
    DateTimeFormatter LONG = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    DateTimeFormatter SHORT = DateTimeFormatter.ofPattern("yyyyMMdd");
    DateTimeFormatter COMPRESS = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
}
