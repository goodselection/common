package com.goodselection.common.constants;

/**
 * @author YaoXunYu
 * created on 02/21/2019
 */
public interface JWTConstants {
    String USER_ID = "user_id";
    String USERNAME = "username";
    String ROLES = "roles";
    String APP_ID = "app_id";
    String APP_NAME = "app_name";
}
