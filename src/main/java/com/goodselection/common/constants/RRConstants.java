package com.goodselection.common.constants;

/**
 * @author YaoXunYu
 * created on 12/27/2018
 * remote resources constants
 */
public interface RRConstants {
    String ROUTE = "route";
    String METHOD = "method";
    String RESOURCE = "resource";

    String RP = "/role_permission";
    //role
    String ROLE = "/role";
    //role-name
    String ROLE_NAME = "/role/name";
    //role-frozen
    String ROLE_FROZEN = "/role/frozen";
    //permission
    String PERMISSION = "/permission";
    //permission-frozen
    String PERMISSION_FROZEN = "/permission/frozen";

    //role-permission
    String ROLE_PERMISSION = "/role/permission";
}
