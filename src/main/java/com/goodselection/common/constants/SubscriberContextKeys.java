package com.goodselection.common.constants;

/**
 * @author YaoXunYu
 * created on 01/14/2019
 */
public interface SubscriberContextKeys {

    String USER_AUTH = "JWT_USER_AUTHENTICATION";

    String APP_AUTH = "JWT_APP_AUTHENTICATION";
}