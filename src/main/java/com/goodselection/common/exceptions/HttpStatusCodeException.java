package com.goodselection.common.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

/**
 * @author YaoXunYu
 * created on 12/24/2018
 */
public abstract class HttpStatusCodeException extends RuntimeException {

    private HttpStatus status;

    public HttpStatusCodeException(HttpStatus status) {
        this.status = status;
    }

    public HttpStatusCodeException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public HttpStatusCodeException(Throwable cause, HttpStatus status) {
        super(cause);
        this.status = status;
    }

    public HttpStatusCodeException(String message, Throwable cause, HttpStatus status) {
        super(message, cause);
        this.status = status;
    }

    public abstract MediaType getContentType();

    public abstract void setContentType(MediaType contentType);

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}
