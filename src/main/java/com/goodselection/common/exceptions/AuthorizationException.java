package com.goodselection.common.exceptions;

import org.springframework.http.HttpStatus;

/**
 * @author YaoXunYu
 * created on 12/24/2018
 */
public class AuthorizationException extends DefaultHttpStatusCodeException {

    public AuthorizationException() {
        super("Access Denied", HttpStatus.FORBIDDEN);
    }

    public AuthorizationException(String message) {
        super(message, HttpStatus.FORBIDDEN);
    }

    public AuthorizationException(Throwable cause) {
        super(cause, HttpStatus.FORBIDDEN);
    }

    public AuthorizationException(String message, Throwable cause) {
        super(message, cause, HttpStatus.FORBIDDEN);
    }
}
