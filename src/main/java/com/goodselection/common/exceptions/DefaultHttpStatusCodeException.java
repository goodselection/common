package com.goodselection.common.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.nio.charset.StandardCharsets;

/**
 * @author YaoXunYu
 * created on 12/24/2018
 */
public class DefaultHttpStatusCodeException extends HttpStatusCodeException {

    private static final MediaType CONTENT_TYPE = new MediaType(MediaType.TEXT_PLAIN, StandardCharsets.UTF_8);

    public DefaultHttpStatusCodeException(HttpStatus status) {
        super(status);
    }

    public DefaultHttpStatusCodeException(String message, HttpStatus status) {
        super(message, status);
    }

    public DefaultHttpStatusCodeException(Throwable cause, HttpStatus status) {
        super(cause, status);
    }

    public DefaultHttpStatusCodeException(String message, Throwable cause, HttpStatus status) {
        super(message, cause, status);
    }

    @Override
    public MediaType getContentType() {
        return CONTENT_TYPE;
    }

    @Override
    public void setContentType(MediaType contentType) {
        throw new RuntimeException("can not set contentType of DefaultHttpStatusCodeException, use HttpStatusCodeException instead");
    }
}
