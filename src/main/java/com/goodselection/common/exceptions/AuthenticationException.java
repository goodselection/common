package com.goodselection.common.exceptions;

import org.springframework.http.HttpStatus;

/**
 * @author YaoXunYu
 * created on 12/24/2018
 */
public class AuthenticationException extends DefaultHttpStatusCodeException {

    public AuthenticationException() {
        super("Access Denied", HttpStatus.UNAUTHORIZED);
    }

    public AuthenticationException(String message) {
        super(message, HttpStatus.UNAUTHORIZED);
    }

    public AuthenticationException(Throwable cause) {
        super(cause, HttpStatus.UNAUTHORIZED);
    }

    public AuthenticationException(String message, Throwable cause) {
        super(message, cause, HttpStatus.UNAUTHORIZED);
    }
}
