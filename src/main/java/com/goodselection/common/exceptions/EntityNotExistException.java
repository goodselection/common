package com.goodselection.common.exceptions;

import org.springframework.http.HttpStatus;

/**
 * @author YaoXunYu
 * created on 01/15/2019
 */
public class EntityNotExistException extends DefaultHttpStatusCodeException {
    public EntityNotExistException() {
        super(HttpStatus.PRECONDITION_FAILED);
    }

    public EntityNotExistException(String message) {
        super(message, HttpStatus.PRECONDITION_FAILED);
    }

    public EntityNotExistException(Throwable cause) {
        super(cause, HttpStatus.PRECONDITION_FAILED);
    }

    public EntityNotExistException(String message, Throwable cause) {
        super(message, cause, HttpStatus.PRECONDITION_FAILED);
    }
}
