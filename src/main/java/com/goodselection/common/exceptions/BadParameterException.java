package com.goodselection.common.exceptions;

import org.springframework.http.HttpStatus;

/**
 * @author YaoXunYu
 * created on 01/07/2019
 */
public class BadParameterException extends DefaultHttpStatusCodeException {
    public BadParameterException() {
        super(HttpStatus.BAD_REQUEST);
    }

    public BadParameterException(String message) {
        super(message, HttpStatus.BAD_REQUEST);
    }

    public BadParameterException(Throwable cause) {
        super(cause, HttpStatus.BAD_REQUEST);
    }

    public BadParameterException(String message, Throwable cause) {
        super(message, cause, HttpStatus.BAD_REQUEST);
    }
}
